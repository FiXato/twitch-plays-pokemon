require 'rubygems'
require 'bundler'
Bundler.require(:default, :test)
require "rspec/expectations"
require_relative '../twitch_plays_pokemon'

# Used http://pastebin.com/YJZbQvjD (as well as discussions on IRC) as basis for these testcases.

describe TwitchPlaysPokemon, "#check" do
  COMMANDS = %w[a b up down left right select start]
  MODES = %w[democracy anarchy]
  before(:each) do
    @r = TwitchPlaysPokemon.new
  end

  describe '#check' do 
    COMMANDS.each do |command|
      it "returns true for '#{command}'-command" do
        @r.check(command).should be_true
      end

      it "returns true for upcased '#{command.upcase}'-command " do
        @r.check(command.upcase).should be_true
      end

      if command.size > 1
        mixed_case_command = command.dup
        mixed_case_command[1] = mixed_case_command[1].upcase
        it "returns true for mixed case '#{mixed_case_command}'-command" do
          @r.check(mixed_case_command).should be_true
        end
      end

      it "returns true if checked line includes '#{command}'-command followed by a digit ranging from 2 to 9 (ex: #{command}2)" do
        (2..9).each do |digit|
          @r.check("#{command}#{digit}").should be_true
        end
      end

      it "returns false if checked line includes '#{command}'-command followed by a digit other than 2 to 9 (ex: #{command}10)" do
        @r.check('%s0' % command).should be_false
        @r.check('%s1' % command).should be_false
        @r.check('%s10' % command).should be_false
        @r.check('%s22' % command).should be_false
      end

      it "returns false for two /the same/ consecutive '#{command}'-commands (ex: '#{'%s%s' % [command,command]}')" do
        @r.check('%s%s' % [command,command]).should be_false
      end

      it "returns false for two /the same/ consecutive '#{command}' 'x-repeated' commands (ex: '#{'%s2%s2' % [command,command]}')" do
        @r.check('%s2%s2' % [command,command]).should be_false
      end

      it "returns false for two /the same/ consecutive '#{command}' commands of which only the first is 'x-repeated' (ex: '#{'%s2%s' % [command,command]}')" do
        @r.check('%s2%s' % [command,command]).should be_false
      end

      it "returns false for two /the same/ consecutive '#{command}' commands of which only the last is 'x-repeated' (ex: '#{'%s%s2' % [command,command]}')" do
        @r.check('%s%s2' % [command,command]).should be_false
      end
    end

    it "returns true for two /different/ consecutive commands (ex: '#{COMMANDS[0,2].join('')}')" do
      @r.check(COMMANDS[0,2].join('')).should be_true
    end

    it "returns false for three consecutive commands of which the last two are /the same/ (ex: '#{COMMANDS[0,2].join('') + COMMANDS[1]}')" do
      @r.check(COMMANDS[0,2].join('') + COMMANDS[1]).should be_false
    end

    it "returns false for four consecutive commands of which the last two are /the same/ (ex: '#{COMMANDS[0,3].join('') + COMMANDS[2]}')" do
      @r.check(COMMANDS[0,3].join('') + COMMANDS[2]).should be_false
    end

    it "returns true for four consecutive commands of which the first two different commands, are also the last two different commands (ex: '#{COMMANDS[0,2].join('') + COMMANDS[0,2].join('')}')" do
      @r.check(COMMANDS[0,2].join('') + COMMANDS[0,2].join('')).should be_true
    end

    it "returns true for two /different/ consecutive commands where the first is an x-repeated command (ex: '#{COMMANDS[0,2].join('2')}')" do
      @r.check(COMMANDS[0,2].join('2')).should be_true
    end

    it "returns true for two /different/ consecutive commands where the second is an x-repeated command (ex: '#{COMMANDS[0,2].join('2')}2')" do
      @r.check(COMMANDS[0,2].join('') + '2').should be_true
    end

    it "returns true for two /different/ consecutive commands where both are an x-repeated command (ex: '#{COMMANDS[0,2].join('2')}2')" do
      @r.check(COMMANDS[0,2].join('2') + '2').should be_true
    end

    it "returns false for double digits after second command (ex: '#{COMMANDS[0,2].join('')}22')" do
      @r.check(COMMANDS[0,2].join('') + '22').should be_false
    end

    it "returns false for double digits after second command (ex: '#{COMMANDS[0,2].join('2')}22')" do
      @r.check(COMMANDS[0,2].join('2') + '22').should be_false
    end

    (3..4).each do |number_of_commands|
      it "returns true if checked line contains #{number_of_commands} commands (ex: '#{COMMANDS[0,number_of_commands].join('')}')" do
        @r.check(COMMANDS[0,number_of_commands].join('')).should be_true
      end

      it "returns true if checked line contains #{number_of_commands} 'x-repeated' commands (ex: '#{COMMANDS[0,number_of_commands].join('2')}')" do
        @r.check(COMMANDS[0,number_of_commands].join('2')).should be_true
        @r.check(COMMANDS[0,number_of_commands].join('2') + '2').should be_true
      end
    end

    it "returns false when it contains more than 4 commands (ex: '#{COMMANDS[0,5].join('')}')" do
      @r.check(COMMANDS[0,5].join('')).should be_false
    end

    it "returns false (each with a number repeat) (ex: '#{COMMANDS[0,5].join('2')}2'" do
      @r.check(COMMANDS[0,5].join('2') + '2').should be_false
    end

    MODES.each do |mode|
      mixed_case_mode = mode.dup
      mixed_case_mode[1] = mixed_case_mode[1].upcase

      it "returns true if checked line is mode #{mode}" do
        @r.check(mode).should be_true
      end

      it "returns true if checked line is mode #{mode} in mixed case (ex: '#{mixed_case_mode}')" do
        @r.check(mixed_case_mode).should be_true
      end

      it "returns false if mode #{mode} is followed by a repeat number (ex: '#{mode}2')" do
        (0..10).each do |digit|
          @r.check("#{mode}#{digit}").should be_false
        end
      end

      it "returns false if another command as well as mode #{mode} is included (ex: '#{COMMANDS.first}#{mode}')" do
        COMMANDS.each do |command|
          @r.check("#{command}#{mode}").should be_false
          @r.check("#{mode}#{command}").should be_false
        end
      end

      it "returns false if mode is repeated (ex: '#{mode}#{mode}')" do
        @r.check("#{mode}#{mode}").should be_false
        @r.check("#{mode}#{mode}#{mode}").should be_false
        @r.check("#{mode}#{mode}#{mode}#{mode}").should be_false
      end
    end
    
    it "returns false if multiple modes are included (ex: '#{MODES.join('')}')" do
      @r.check(MODES.join('')).should be_false
    end
  end

  describe '#all_commands' do 
    it 'should return an array of all commands in the log line' do
      @r.all_commands(COMMANDS.join('')).should eq(COMMANDS)
    end

    it 'should return an array of all modes in the log line' do
      @r.all_commands(MODES.join('')).should eq(MODES)
    end
  end
  
  describe '#check_and_return_all_commands' do
    it 'should return nil if log line fails #check' do
      log_line = 'aa'
      @r.should_receive(:check).with(log_line) {false}
      @r.check_and_return_all_commands(log_line).should be_nil
    end

    it 'should return #all_commands if log line passes #check' do
      log_line = 'a2b2'
      expected_commands = %w[a2 b2]
      @r.should_receive(:check).with(log_line) {true}
      @r.should_receive(:all_commands).with(log_line) {expected_commands}
      @r.check_and_return_all_commands(log_line).should eq(expected_commands)
    end
  end
end