#!/usr/bin/env ruby
#encoding: utf-8
class TwitchPlaysPokemon
  LOGLINE_REGEX = /^(?i:((?<command>a|b|up|down|left|right|start|select)[2-9]?(?!\k<command>)){1,4}|democracy|anarchy)$/ #thanks @nasonfish!
  # LOGLINE_REGEX = /^(?i:(?<cmd1complete>(?<cmd1>a|b|up|down|left|right|start|select)(?<cmd1quantifier>[2-9]?)(?!\k<cmd1>))(?<cmd2complete>(?<cmd2>a|b|up|down|left|right|start|select)(?<cmd2quantifier>[2-9]?))?(?!\k<cmd2>)(?<cmd3complete>(?<cmd3>a|b|up|down|left|right|start|select)(?<cmd3quantifier>[2-9]?))?(?!\k<cmd3>)(?<cmd4complete>(?<cmd4>a|b|up|down|left|right|start|select)(?<cmd4quantifier>[2-9]?))?(?!\k<cmd4>)|(?<mode>democracy|anarchy))$/ #Alternative solution that allows to capture each command, quantifier, complete command and mode from a single regex. Far less legible though.
  COMMANDS_REGEX = /(?i:anarchy|democracy|(?:a|b|up|down|left|right|start|select)[2-9]?)/
  def check(log_line)
    !LOGLINE_REGEX.match(log_line).nil?
  end

  def all_commands(log_line)
    matches = log_line.scan(COMMANDS_REGEX)
  end
  
  def check_and_return_all_commands(log_line)
    return nil unless check(log_line)
    all_commands(log_line)
  end
end